package no.noroff.MovieCharacterAPI.repositories;

import no.noroff.MovieCharacterAPI.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}
